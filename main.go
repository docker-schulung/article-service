package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/gossie/router"
)

type article struct {
	Id          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

var articlesById map[string]article = map[string]article{
	"4711": {"4711", "Artikel 4711", "Die Beschreibung für Artikel 4711"},
	"4712": {"4712", "Artikel 4712", "Die Beschreibung für Artikel 4712"},
	"4713": {"4713", "Artikel 4713", "Die Beschreibung für Artikel 4713"},
}

func healthy() bool {
	_, err := os.ReadFile("/usr/local/article-service/unhealthy.txt")
	if err == nil {
		log.Default().Println("unhealthy.txt is present => service is unhealthy")
		return false
	}
	log.Default().Println("unhealthy.txt is not present => service is healthy")
	return true
}

func readJsonIfPresent() {
	content, err := os.ReadFile("/usr/local/article-service/data.json")
	if err == nil {
		var articles []article
		err := json.Unmarshal(content, &articles)
		if err == nil {
			clear(articlesById)
			log.Default().Println("Importing", len(articles), "articles")
			for i := range articles {
				a := articles[i]
				articlesById[a.Id] = a
				log.Default().Println(a, "was imported")
			}
		} else {
			log.Default().Println("JSON could not be parsed, default prices are used:", err.Error())
		}
	} else {
		log.Default().Println("No data.json present, default prices are used")
	}
}

func main() {
	readJsonIfPresent()

	httpRouter := router.New()

	httpRouter.Use(func(hh router.HttpHandler) router.HttpHandler {
		return func(w http.ResponseWriter, r *http.Request, ctx router.Context) {
			w.Header().Add("Content-Type", "application/json")
			hh(w, r, ctx)
		}
	})

	httpRouter.Get("/articles", func(w http.ResponseWriter, r *http.Request, ctx router.Context) {
		log.Default().Println("Retrieve all articles")
		articles := make([]article, 0, len(articlesById))
		for _, a := range articlesById {
			articles = append(articles, a)
		}

		j, _ := json.Marshal(articles)
		w.Write(j)
	})

	httpRouter.Get("/health", func(w http.ResponseWriter, r *http.Request, ctx router.Context) {
		if len(articlesById) == 0 {
			log.Default().Println("No articles present => service is unhealthy")
			http.Error(w, "unhealthy", http.StatusInternalServerError)
			return
		}

		if !healthy() {
			http.Error(w, "unhealthy", http.StatusInternalServerError)
			return
		}

		w.WriteHeader(200)
		w.Write([]byte("healthy"))
	})

	httpRouter.Get("/articles/:id", func(w http.ResponseWriter, r *http.Request, ctx router.Context) {
		articleId := ctx.PathParameter("id")
		log.Default().Println("Retrieve article with ID", articleId)
		a, present := articlesById[articleId]
		if !present {
			http.Error(w, "Article with ID "+articleId+" not found", http.StatusNotFound)
			return
		}

		j, _ := json.Marshal(&a)
		w.Write(j)
	})

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	log.Default().Println("running on PORT", port)
	log.Fatal(http.ListenAndServe(":"+port, httpRouter))
}
